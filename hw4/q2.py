from functools import reduce
from operator import mul

def prod(iterable):
    return reduce(mul, iterable, 1)


# 高興(h)和不高興(~h)文件的數量
num_h = 4
num_nh = 3

# 高興(h)和不高興(~h)文件的詞數
num_words_h = 21
num_words_nh = 10

# 每個詞在高興(h)和不高興(~h)文件中出現的次數
word_counts_h = {'love': 6, 'happy': 6, 'joy': 3, 'smile': 1, 'angry': 1, 'cry': 2, 'good': 1, 'pain': 1}
word_counts_nh = {'love': 2, 'angry': 4, 'pain': 4}

# 計算每個詞在高興(h)和不高興(~h)文件中出現的機率
word_probs_h = {word: (count + 1) / (num_words_h + len(word_counts_h)) for word, count in word_counts_h.items()}
word_probs_nh = {word: (count + 1) / (num_words_nh + len(word_counts_nh)) for word, count in word_counts_nh.items()}
print(word_probs_h)
print(word_probs_nh)

# 計算文件為高興(h)和不高興(~h)的機率
prob_h = num_h / (num_h + num_nh)
prob_nh = num_nh / (num_h + num_nh)
print(prob_h)
print(prob_nh)

# 計算第八份文件(Test Document)為高興(h)和不高興(~h)的機率
test_doc = ['cry', 'love', 'angry', 'pain']
prob_doc_h = prob_h * prod(word_probs_h.get(word, 1 / (num_words_h + len(word_counts_h))) for word in test_doc)
prob_doc_nh = prob_nh * prod(word_probs_nh.get(word, 1 / (num_words_nh + len(word_counts_nh))) for word in test_doc)
print(prob_doc_h)
print(prob_doc_nh)

# 判斷文件類型
doc_type = 'Happy(h)' if prob_doc_h > prob_doc_nh else 'Unhappy(~h)'

print(doc_type)
